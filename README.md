Modern C++ with Threads Programming, 5 days
====

Welcome to this course. 

Here you will find
* Installation instructions
* Solutions to the programming exercises
* Sources to the demo programs

Installation Instructions
====

To perform the programming exercises, you need:
* An C++ IDE, to write your code
* A modern C++ compiler, supporting C++ 17, to compile your code
* A GIT client, to get the solutions from this repo

Recommended Setup
----
Although, there are many ways to write and compile C++ programs both on Windows,
Linux and Mac, our experience of giving many courses on various levels; is that
using Ubuntu Linux provides the least amount of surprises and distracting
technical struggles.

For this course, we do recommend the following setup:
* Ubuntu Linux, version 18.04 or later
* GCC C++ compiler, version 8
* [JetBrains CLion IDE, 30-days trial](https://www.jetbrains.com/clion/download/)

Ubuntu Linux (WSL) @ Windows 10
----

One of the biggest news with Windows 10, is that it provides the _Windows Subsystem for
Linux_ (WSL) plugin. Just open _Microsoft Store_ and search for "Ubuntu". Then choose
Ubuntu 18.04 and install it. It's as simple as that.

Our favorite C++ IDE is CLion and it works very nicely with WSL! You install CLion for 
_Windows_ and then setup a SSH connection to WSL, which uses the compiler resources in Ubuntu.
This is the setup I will be using during the course.

Just follow the  instructions at
* [How to Use WSL Development Environment in CLion](https://www.jetbrains.com/help/clion/how-to-use-wsl-development-environment-in-clion.html)
* [Using WSL toolchains in CLion on Windows (YouTube)](https://youtu.be/xnwoCuHeHuY)

Ubuntu Linux @ VirtualBox
----

If you're running an earlier version of MS Windows, such as version 7, then install VirtualBox, create a virtual machine for Linux and download/install an Ubuntu ISO to it.

Here are the steps in more detail:

1. Install VirtualBox (VBox)<br/>
    <https://www.virtualbox.org/wiki/Downloads>
1. Create a new virtual machine (VM) i VBox, for Ubuntu Linux<br/>
    <https://www.virtualbox.org/manual/ch03.html>
1. Download an ISO file for the latest version of Ubuntu Desktop 64-bit<br/>
    <http://www.ubuntu.com/download/desktop>
1. Choose as much RAM for the VM as you can, still within the "green" region.
1. Choose as much video memory you can
1. Create an auto-expanding virtual hard-drive of at least 50GB (_N.B. unless you use up all disk space within Linux, it will not occupy that much space on Windows_)
1. Mount the ISO file in the virtual CD drive of your VM
1. Start the VM and run the Ubuntu installation program.
    Ensure you install to the (virtual) hard-drive.
1. Set a username and password when asked to and write them down so you remember them.
   Also, go for auto-logon.
1. Install the VBox guest additions<br/>
    <https://www.virtualbox.org/manual/ch04.html>
1. Install Clion from the Ubuntu programs installer app

GCC C++ Compiler and Tools
----
Within a Ubuntu terminal window type the following command to install the
compiler and other tools.

    sudo apt install g++-8 cmake make gdb valgrind git tree

_N.B._ when you run a `sudo` command it prompts you for the password, you use
to logon to Ubuntu.

Mac User
----
If you have a Mac laptop, install a Modern C++ compiler supporting C++ 17
and install JetBrains CLion.

Linux User
----
If you're running a different Linux desktop OS, such as Fedora, Suse or similar; use its
package manager to install a compiler supporting C++ 17 and then install  CLion.

Writing your own solutions
====
During the course, you will be asked to implement many small programs, as part of the training
exercise. I do recommend to create a dedicated directory for this course. Here is a suggested structure:

    cxx-course/
        course-material.pdf
        my-solutions/
            ch01/
            ch02/
            ...
        gitlab/
            solutions/
            demo/
            files/

Have a sub-directory for your solutions and another sub-directory for mine solutions.
In addition, download and save the course material PDF. You will be provided the URL to the
PDF at course start.



Usage of this GIT Repo
====
Ensure you have a [GIT client](https://git-scm.com/downloads) installed and clone
this repo. The repo is public, which means you do not have to signup for an account
at GitLab. If git prompts you for credentials, it usually means the url is misspelled.

    mkdir -p ~/cxx-course/my-solutions
    cd ~/cxx-course
    git clone https://gitlab.com/ribomation-courses/cxx/cxx-supplementary-with-threads.git gitlab


During the course, solutions will be push:ed to this repo and you can get these by
a `git pull` operation

    cd ~/cxx-course/gitlab
    git pull


Build Solution/Demo Programs
====
The solutions and demo programs are all using CMake as the build tool. CMake is a cross
platform generator tool that can generate makefiles and other build tool files. It is also
the project descriptor for JetBrains CLion, which is my IDE of choice for C/C++ development.

You don't have to use CLion in order to compile and run the sources. What you do need; 
are `cmake`, `make` and `gcc/g++` all installed. When you want to build a solution or demo; 
First change into its project directory, then run the  commands below.

    cd path/to/some/solutions/dir
    mkdir -p build && cd $_
    cmake -G 'Unix Makefiles' ..
    cmake --build .

The executable is now in the `./build/` directory.


Using Windows for the Course
====
You can write your solutions for this course using
*MS Visual Studio* running on Windows. However, the majority of your customers are targeting *NIX systems, which is why we primary recommend using an environment such as Ubuntu Linux.

Any way, if you already feel comfortable using MS Visual Studio, then use it for this course.
* [MS Visual Studio, overview & download](https://www.visualstudio.com/vs/)

Interesting Links
====
* [CppCon 2018: Bjarne Stroustrup “Concepts: The Future of Generic Programming (the future is here)”](https://youtu.be/HddFGPTAmtU)
* [CppCon 2015: Eric Niebler "Ranges for the Standard Library"](https://youtu.be/mFUXNMfaciE)
* [CppCon 2015: Bjarne Stroustrup “Writing Good C++14”
](https://youtu.be/1OEu9C51K2A)
* [C++ Core Guidelines](http://isocpp.github.io/CppCoreGuidelines/CppCoreGuidelines)
* [CppReference](https://en.cppreference.com/w/)


***
*If you have any questions, don't hesitate to contact me*<br>
**Jens Riboe**<br/>
Ribomation AB<br/>
[jens.riboe@ribomation.se](mailto:jens.riboe@ribomation.se)<br/>
[www.ribomation.se](https://www.ribomation.se)<br/>
