#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include <queue>
#include <algorithm>
#include <thread>
#include "MessageQueue.hxx"

using namespace std;
using XXL = unsigned long long;

void Producer(unsigned N, MessageQueue<XXL>* out) {
    for (XXL k = 1; k <= N; ++k) {
        *out << k;
    }
    *out << 0;

    ostringstream buf;
    buf << "[producer] done\n";
    cout << buf.str();
}

void Transformer(unsigned id, MessageQueue<XXL>* in, MessageQueue<XXL>* out) {
    for (XXL msg; (*in >> msg) > 0;) {
        *out << 2 * msg;
    }
    *out << 0;

    ostringstream buf;
    buf << "[transformer-" << id << "] done\n";
    cout << buf.str();
}

void Consumer(MessageQueue<XXL>* in) {
    for (XXL      msg; (*in >> msg) > 0;) {
        ostringstream buf;
        buf << "[consumer] " << msg << "\n";
        cout << buf.str();
    }
    ostringstream buf;
    buf << "[consumer] done\n";
    cout << buf.str();
}


bool operator <<(const string& target, const string& prefix) {
    return target.substr(0, prefix.size()) == prefix;
}

unsigned stou(const string& txt) {
    return static_cast<unsigned>(stoul(txt));
}

int main(int numArgs, char* args[]) {
    using namespace string_literals;
    const auto options              = " [--transformers={int}] [--messages={int}]"s;
    auto       numTransformers      = 10U;
    auto       numMessages          = 30U;

    for_each(args + 1, args + numArgs, [&](string arg) -> void {
        if (arg << "--transformers="s) {
            numTransformers = stou(arg.substr(15));
        } else if (arg << "--messages="s) {
            numMessages = stou(arg.substr(11));
        } else {
            cerr << "unknown option: " << arg << endl;
            cerr << "usage: " << args[0] << options << endl;
            ::exit(1);
        }
    });

    vector<MessageQueue<XXL>> queues(numTransformers + 1);
    unsigned long             mbIdx = queues.size() - 1;

    thread c(Consumer, &queues[mbIdx]);

    unsigned id = 1;
    while (numTransformers-- > 0) {
        thread t(Transformer, id++, &queues[mbIdx - 1], &queues[mbIdx]);
        t.detach();
        --mbIdx;
    }

    thread   p(Producer, numMessages, &queues[0]);
    p.join();
    c.join();

    return 0;
}