cmake_minimum_required(VERSION 3.12)
project(ch3_syntax LANGUAGES CXX)

set(CMAKE_CXX_STANDARD 17)

add_executable(app app.cxx)

