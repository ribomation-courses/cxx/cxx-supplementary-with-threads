#include <iostream>
using namespace std;

int main() {
    cout << "dec(42)    : " << 42U << endl;
    cout << "hex(2A)    : " << 0x2A << endl;
    cout << "oct(52)    : " << 052 << endl;
    cout << "bin(101010): " << 0b0010'1010 << endl;
    return 0;
}
