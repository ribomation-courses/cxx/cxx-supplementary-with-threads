#include <iostream>
#include <string>
#include <vector>
#include <cassert>

using namespace std;
using namespace std::literals;

struct Data {
    unsigned ival;
    float    fval;
};

int main() {
    {
        int num{};
        assert(num == 0);
    }

    {
        int num{42};
        assert(num == 42);
    }

    {
        string s{"hello"};
        assert(s == "hello"s);
    }

    {
        vector<string> words = {"C++", "is", "a", "cool", "language"};
        assert(words.size() == 5);
    }
    {
        Data d{};
        assert(d.ival == 0U);
        assert(d.fval == 0.F);
    }

    {
        string haystack = "Foobar strikes again and again"s;
        if (auto pos = haystack.rfind("aga"s); pos != string::npos) {
            string s = haystack.substr(pos);
            assert(s == "again"s);
        }
    }

    cout << "All tests passed!\n";
    return 0;
}