#include <iostream>
#include <functional>
using namespace std;

auto reduce(int* arr, unsigned N, function<int(int, int)> f)  {
    auto result = arr[0];
    for (auto k = 1U; k < N; ++k) result = f(result, arr[k]);
    return result;
}

int main() {
    int        numbers[] = {6, 1, 8, 9, 2, 10, 3, 4, 5, 7};
    auto const N         = sizeof(numbers) / sizeof(numbers[0]);

    auto sum = reduce(numbers, N, [](auto acc, auto n) { return acc + n; });
    auto prd = reduce(numbers, N, [](auto acc, auto n) { return acc * n; });
    auto max = reduce(numbers, N, [](auto acc, auto n) { return acc > n ? acc : n; });

    cout<< "sum      : " << sum << endl;
    cout<< "product  : " << prd << endl;
    cout<< "max value: " << max << endl;
    auto ptr = new int{42};
    delete ptr;
    return 0;
}
