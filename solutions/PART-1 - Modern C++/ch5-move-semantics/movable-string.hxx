#pragma once

#include <iostream>
#include <cstring>

namespace ribomation {
    using namespace std;

    class TinyString {
        const char* payload = nullptr;

    public:
        TinyString() = delete;
        TinyString(const TinyString&) = delete;
        TinyString& operator =(const TinyString&) = delete;

        TinyString(const char* s) : payload{strdup(s)} {
            cout << "TinyString(char* '" << payload << "') @ " << this << endl;
        }

        ~TinyString() {
            cout << "~TinyString() @ " << this;
            if (payload) {
                cout << " DELETE \"" << payload << "\" @ " << reinterpret_cast<unsigned long>(payload);
                delete payload;
            } else {
                cout << " no payload";
            }
            cout << endl;
        }

        TinyString(TinyString&& that) : payload(that.payload) {
            cout << "TinyString(&& '" << payload << "') @ " << this << endl;
            that.payload = nullptr;
        }

        TinyString& operator =(TinyString&& that) {
            cout << "TinyString::operator=(&& '" << payload << "') @ " << this;
            if (this != &that) {
                if (payload) {
                    cout << " DELETE \"" << payload << "\"";
                    delete payload;
                }
                payload = that.payload;
                that.payload = nullptr;
            }
            cout << endl;
            return *this;
        }


        friend std::ostream& operator <<(std::ostream& os, const TinyString& s) {
            return os << '"' << (s.payload != nullptr ? s.payload : "") << "\" @ "
                      << reinterpret_cast<unsigned long>(s.payload);
        }

        friend TinyString operator +(const TinyString& lhs, const TinyString& rhs) {
            char* buf = new char[1 + strlen(lhs.payload) + strlen(rhs.payload)];
            strcpy(buf, lhs.payload);
            strcat(buf, rhs.payload);
            return {buf};
        }
    };
}
