#include <iostream>
#include <fstream>
#include <string>
#include <stdexcept>
#include <set>
#include <unordered_set>
#include <algorithm>
#include <cctype>

using namespace std;
using namespace std::literals;


auto load(istream& in, unsigned minWordSize) {
    unordered_multiset<string> words;

    for (string word; in >> word;) {
        word.erase(remove_if(word.begin(), word.end(), [](auto ch) {
            return !isalpha(ch);
        }), word.end());

        if (word.size() >= minWordSize) {
            transform(begin(word), end(word), begin(word), [](auto ch) {
                return tolower(ch);
            });
            words.insert(word);
        }
    }

    return words;
}

void print(const multiset<string>& freqs) {
    for (auto it = freqs.begin(); it != freqs.end();) {
        auto word = *it;
        auto freq = freqs.count(word);
        cout << word << ": " << freq << endl;
        advance(it, freq);
    }
}

int main(int argc, char** argv) {
    const auto filename     = (argc == 1) ? "./musketeers.txt"s : argv[1];
    const auto minWordSize  = 4U;

    auto file = ifstream{filename};
    if (!file) throw invalid_argument{"cannot open "s + filename};

    auto words = load(file, minWordSize);
    auto freqs = multiset<string>{words.begin(), words.end()};
    print(freqs);

    return 0;
}

