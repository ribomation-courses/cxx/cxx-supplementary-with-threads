#include <iostream>
#include <string>
#include <tuple>
#include <stdexcept>

using namespace std;
using namespace std::literals;

int main(int numArgs, char* args[]) {
    if (numArgs <= 1) {
        throw invalid_argument("usage: "s + args[0] + " {phrase} < {input}"s);
    }
    string phrase = args[1];
    for (auto[lineno, line] = make_tuple(1U, ""s);
         getline(cin, line);
         ++lineno) {
        if (line.find(phrase) != string::npos) {
            cout << lineno << ": " << line << endl;
        }
    }
    return 0;
}
