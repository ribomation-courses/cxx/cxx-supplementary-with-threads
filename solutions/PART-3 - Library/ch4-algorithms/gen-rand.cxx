#include <iostream>
#include <string>
#include <random>
#include <algorithm>
#include <iterator>

using namespace std;
using namespace std::literals;


int main(int argc, char** argv) {
    auto n = argc == 1 ? 10U : stoi(argv[1]);

    random_device r;  //dev/random
    normal_distribution<double> gauss{5, 5};
    generate_n(ostream_iterator<double>{cout, "\n"}, n, [&]{
        return gauss(r);
    });

    return 0;
}

