#include <iostream>
#include <chrono>

using namespace std;
using namespace std::literals;

int main() {
    auto duration = 3h + 5min + 49s;
    cout << "duration: " << duration.count() << " seconds\n";
    return 0;
}