#pragma once

#include <string>
#include <optional>

namespace ribomation {
    using namespace std;
    using namespace std::literals;

    struct Address {
        optional<string>   street;
        optional<unsigned> postCode;
        optional<string>   city;
    };

}
