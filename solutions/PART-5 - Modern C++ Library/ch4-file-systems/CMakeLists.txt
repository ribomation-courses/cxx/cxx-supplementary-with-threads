cmake_minimum_required(VERSION 3.10)
project(ch17_file_systems)

set(CMAKE_CXX_STANDARD 17)
add_compile_options(-Wall -Wextra -Werror -Wfatal-errors)

add_executable(dir-count dir-count.cxx count.hxx)
target_link_libraries(dir-count stdc++fs)
