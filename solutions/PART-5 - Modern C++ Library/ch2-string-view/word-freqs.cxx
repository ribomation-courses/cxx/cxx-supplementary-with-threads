#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include <string_view>
#include <stdexcept>
#include <vector>
#include <algorithm>
#include <tuple>
#include <utility>
#include <map>
#include <cstring>
#include <cerrno>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>

using namespace std;
using namespace std::literals;

auto load(const string& filename) -> tuple<const char*, unsigned long> {
    struct stat info{};
    if (stat(filename.data(), &info) == -1) {
        throw invalid_argument{"stat failed: "s + strerror(errno)};
    }
    auto size = info.st_size;

    char* payload = new char[size + 1];
    auto fd = open(filename.c_str(), O_RDONLY);
    read(fd, payload, static_cast<size_t>(size + 1));
    close(fd);
    payload[size] = '\0';

    return {payload, size};
}

auto count(const char* payload, unsigned long size) -> map<string_view, unsigned> {
    map<string_view, unsigned> freqs;

    auto start = 0UL;
    do {
        while (!isalpha(payload[start]) && (start < size)) ++start;

        auto end = start;
        while (isalpha(payload[end]) && (start < size)) ++end;

        string_view word{&payload[start], end - start};
        if (word.size() > 3) ++freqs[word];

        start = end + 1;
    } while (start < size);

    return freqs;
}

int main(int argc, char** argv) {
    auto const filename = (argc == 1) ? "../musketeers.txt"s : argv[1];

    auto[payload, size] = load(filename);
    cout << "file.size = " << size << endl;

    auto freqs = count(payload, size);
    cout << "freqs.size= " << freqs.size() << endl;
    cout << "-----------------\n";

    using WordFreq = pair<string_view, unsigned>;
    vector<WordFreq> sortable{freqs.begin(), freqs.end()};
    sort(sortable.begin(), sortable.end(), [](WordFreq lhs, WordFreq rhs) {
        return lhs.second > rhs.second;
    });

    unsigned cnt = 1;
    for (auto [word, freq] : sortable) {
        cout << setw(12) << word << ": " << freq << endl;
        if (++cnt > 100) break;
    }

    return 0;
}

