cmake_minimum_required(VERSION 3.10)
project(ch11_string_views)

set(CMAKE_CXX_STANDARD 17)
add_compile_options(-Wall -Wextra -Werror -Wfatal-errors)

add_executable(word-freqs word-freqs.cxx)

