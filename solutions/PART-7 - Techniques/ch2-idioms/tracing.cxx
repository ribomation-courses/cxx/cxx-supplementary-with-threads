#include <iostream>
#include <string>

using namespace std;
using namespace std::literals;

class Trace {
    const string        name;
    const unsigned long address;
    bool                hasReturned = false;

public:
    Trace(string name, void* addr) : name{name}, address{reinterpret_cast<unsigned long>(addr)} {
        cout << "CREATE " << name << " @ " << address << endl;
    }

    Trace(string name) : name{name}, address{0} {
        cout << "ENTER " << name << endl;
    }

    Trace(const Trace&) = default;

    ~Trace() {
        if (hasReturned) return;
        if (address == 0) {
            cout << "EXIT " << name << endl;
        } else {
            cout << "DISPOSE " << name << " @ " << address << endl;
        }
    }

    template<typename T>
    T RETURN(T x) {
        cout << "RETURN " << name << " -> " << x << endl;
        hasReturned = true;
        return x;
    }
};

class Person {
    Trace  t;
    string name;
public:
    Person(string s) : t{"Person"s, this}, name{s} {}

    friend ostream& operator <<(ostream& os, const Person& p) {
        return os << "Person{" << p.name << "}";
    }
};

long sum(unsigned n) {
    Trace t{"sum"};
    return t.RETURN(n * (n + 1) / 2);
}


int main(int argc, char** argv) {
    Trace t{"main"};
    {
        Trace  t{"inner"};
        Person p{"Nisse"};
        cout << "p: " << p << endl;
    }
    auto  result = sum(10);
    cout << "SUM(1..10) = " << result << endl;
    return 0;
}
