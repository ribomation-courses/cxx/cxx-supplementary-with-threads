#include <stdio.h>
#include <stdlib.h>

struct Person;

extern const char*      _Z11toUpperCasePKc(const char*);
extern struct Person*   _Z12createPersonPKcj(const char*, unsigned);
extern const char*      _ZNK6Person7getNameEv(struct Person*);
extern unsigned         _ZNK6Person6getAgeEv(struct Person*);
extern unsigned         _ZN6Person7incrAgeEv(struct Person*);
extern void*            _ZN6PersonD1Ev(struct Person*);

int main() {
    const char* s = _Z11toUpperCasePKc("Tjolla Hopp");
    printf("[C] s: %s\n", s);
    free((void*) s);

    struct Person* p = _Z12createPersonPKcj("Anna Conda", 42);
    printf("[C] Person{%s, %d}\n", _ZNK6Person7getNameEv(p), _ZNK6Person6getAgeEv(p));
    printf("[C] Person{%s, %d}\n", _ZNK6Person7getNameEv(p), _ZN6Person7incrAgeEv(p));
    free(_ZN6PersonD1Ev(p));

    return 0;
}

