#include <iostream>
#include <algorithm>
#include <string>
#include <cstring>
#include <cctype>
#include "cxx-lib.hxx"

const char* toUpperCase(const char* s) {
    const auto size = strlen(s);
    char* buf = new char[size + 1];
    std::transform(s, s + size, buf, [](auto ch) {
        return ::toupper(ch);
    });
    buf[size] = '\0';
    return buf;
}

Person::Person(std::string name, unsigned age) : name{name}, age{age} {
    std::cout << "Person(" << name << ", " << age << ") @ " << this << std::endl;
}

Person::~Person() {
    std::cout << "~Person() @ " << this << std::endl;
}

Person* createPerson(const char* s, unsigned age) {
    std::string name{s};
    return new Person{name, age};
}

const char* Person::getName() const {
    return name.c_str();
}

unsigned Person::getAge() const {
    return age;
}

unsigned Person::incrAge() {
    return ++age;
}
