#pragma once

#include <string>

extern const char* toUpperCase(const char* s);

class Person {
    const std::string name;
    unsigned     age;
public:
    Person(std::string name, unsigned age);
    ~Person();
    const char* getName() const;
    unsigned getAge() const;
    unsigned incrAge();
};

