#pragma once

#include <pthread.h>

namespace cxxThreads {

    class Thread {
        pthread_t thrId{};

        void* body() {
            run();
            return nullptr;
        }

    protected:
        virtual void run() = 0;

    public:
        void start() {
            using BodyFnType = void* (*)(void*);
            BodyFnType bodyFn = reinterpret_cast<BodyFnType>(&Thread::body);
            pthread_create(&thrId, nullptr, bodyFn, this);
        }

        void join() {
            pthread_join(thrId, nullptr);
        }

        Thread() = default;
        virtual ~Thread() = default;
        Thread(const Thread&) = delete;
        Thread& operator =(const Thread&) = delete;

        Thread(Thread&& that) noexcept : thrId{that.thrId} {}
    };

}

