#include <iostream>
#include <vector>
#include "Hello-2.hxx"

using namespace std;
using namespace std::literals;
using namespace ribomation;


int main(int numArgs, char* args[]) {
    auto numThreads  = 12U;
    auto numMessages = 5'000U;

    for (auto k      = 1; k < numArgs; ++k) {
        string arg = args[k];
        if (arg == "-m") {
            numMessages = static_cast<unsigned>(stoi(args[++k]));
        } else if (arg == "-t") {
            numThreads = static_cast<unsigned>(stoi(args[++k]));
        }
    }

    vector<Hello> threads;
    for (unsigned id = 1; id <= numThreads; ++id) {
        threads.emplace_back(id, numMessages);
    }
    for (auto& t : threads) t.start();
    for (auto& t : threads) t.join();

    return 0;
}
