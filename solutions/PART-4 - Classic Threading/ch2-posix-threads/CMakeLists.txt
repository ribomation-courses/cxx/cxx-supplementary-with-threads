cmake_minimum_required(VERSION 3.10)
project(03_posix_threads LANGUAGES CXX)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_CXX_EXTENSIONS OFF)
set(CXX_OPTS -Wall -Wextra -Werror -Wfatal-errors)

add_executable(hello-threads
        Thread.hxx
        Hello.hxx
        hello-threads.cxx)
target_compile_options(hello-threads PRIVATE ${CXX_OPTS})
target_link_libraries(hello-threads pthread)


add_executable(hello-threads-2
        Thread-2.hxx
        Hello-2.hxx
        hello-threads-2.cxx)
target_compile_options(hello-threads-2 PRIVATE ${CXX_OPTS} -Wno-pmf-conversions)
target_link_libraries(hello-threads-2 pthread)

