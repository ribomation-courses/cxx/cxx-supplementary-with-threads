#pragma once

#include <iosfwd>
#include <sstream>
#include "Thread.hxx"

namespace ribomation {
    using namespace std;
    using namespace std::literals;
    using namespace cxxThreads;

    class Hello : public Thread {
        const unsigned id;
        const unsigned N;

        string makeName(unsigned id) {
            ostringstream buf;
            buf << string((id - 1) * 4, ' ') << "[" << id << "] ";
            return buf.str();
        }

    public:
        Hello(unsigned id, unsigned N) : id{id}, N{N} {}

    protected:
        void run() override {
            auto name = makeName(id);
            cout << (name + "started\n"s);
            for (unsigned k = 1; k <= N; ++k) cout << (name + "\n"s);
            cout << (name + "done\n"s);
        }
    };

}