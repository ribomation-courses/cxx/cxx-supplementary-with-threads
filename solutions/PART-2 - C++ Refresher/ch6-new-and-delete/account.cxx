#include <iostream>
#include <cstdlib>
#include "account.hxx"

namespace ribomation {
    using namespace std;
    using namespace std::literals;

    double Account::getBalance() const {
        return balance;
    }

    void Account::setBalance(double balance) {
        Account::balance = balance;
    }

    void* Account::operator new(size_t numBytes) {
        auto ptr = malloc(numBytes);
        cout << "new: size=" << numBytes << " --> " << ptr << endl;
        return ptr;
    }

    void Account::operator delete(void* ptr) {
        cout << "delete: ptr=" << ptr << endl;
        free(ptr);
    }

    Account::Account() {
        cout << "Account() @ " << this << endl;
    }

    Account::~Account() {
        cout << "~Account() @ " << this << endl;
    }

}


