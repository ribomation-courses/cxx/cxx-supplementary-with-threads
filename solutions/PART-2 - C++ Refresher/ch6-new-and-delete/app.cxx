#include <iostream>
#include <string>
#include "account.hxx"

using namespace std;
using namespace std::literals;
using namespace ribomation;


int main() {
    auto* acc = new Account{};
    cout << "acc: " << *acc << endl;
    acc->setBalance(100);
    cout << "acc: " << *acc << endl;
    delete acc;
    return 0;
}
