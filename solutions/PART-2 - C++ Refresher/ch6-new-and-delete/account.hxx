#pragma  once

#include <iosfwd>
#include <sstream>
#include <iomanip>
#include <string>
#include <cstddef>

namespace ribomation {
    using namespace std;
    using namespace std::literals;

    class Account {
        double balance = 0;
        string accno = "1234-56789123"s;

    public:
        Account();
        ~Account();
        Account(const Account&) = default;

        double getBalance() const;
        void setBalance(double balance);

        void* operator new(size_t numBytes);
        void operator delete(void* ptr);

        string toString() const {
            ostringstream buf{};
            buf << "Account{SEK " << fixed << setprecision(2) << balance << "}";
            return buf.str();
        }
    };

    inline ostream& operator << (ostream& os, const Account& a) {
        return os << a.toString();
    }
}

