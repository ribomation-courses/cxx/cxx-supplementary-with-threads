#include <iostream>
#include <sstream>
#include <string>
#include "vector-3d.hxx"

using namespace std;
using namespace std::literals;
using namespace ribomation;

void test_int_vector() {
    cout << "--- 3D vec<int> ---\n";
    Vector3d<> v1{2, 5, 10};
    cout << "v1: " << v1 << endl;

    istringstream{"data: <10 20 30>"} >> v1;
    cout << "v1: " << v1 << endl;

    cout << "v1 * 42: " << v1 * 42 << endl;
    cout << "10 * v1: " << 10 * v1 << endl;

    Vector3d<> v2{2, 2, 2};
    cout << "v1 + v2: " << v1 + v2 << endl;
    cout << "v1 - v2: " << v1 - v2 << endl;
    cout << "v1 * v2: " << v1 * v2 << endl;
}

void test_longdouble_vector() {
    cout << "--- 3D vec<long double> ---\n";
    using BigVec = Vector3d<long double>;
    BigVec v1{2, 5, 10};
    cout << "v1: " << v1 << endl;

    istringstream{"data: <10 20 30>"} >> v1;
    cout << "v1: " << v1 << endl;

    cout << "v1 * 42: " << v1 * 42.25L << endl;
    cout << "10 * v1: " << 10.333L * v1 << endl;

    BigVec v2{2, 2, 2};
    cout << "v1 + v2: " << v1 + v2 << endl;
    cout << "v1 - v2: " << v1 - v2 << endl;
    cout << "v1 * v2: " << v1 * v2 << endl;
}

void test_non_numeric() {
    //Vector3d<string>  v;
}

int main() {
    test_int_vector();
    test_longdouble_vector();
    test_non_numeric();
    return 0;
}
