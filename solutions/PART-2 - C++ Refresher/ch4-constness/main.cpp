#include <iostream>
#include <string>

using namespace std;
using namespace std::literals;

struct Data {
    Data() = default;

    void func() const {
     // ++value; //error: increment of member Data::value in read-only object
        ++value2;
    }

    string toString() const {
        return "Data{"s + to_string(value) + ", "s + to_string(value2) + "}"s;
    }
private:
    int         value  = 10;
    mutable int value2 = 50;
};

int main() {
    auto const PI = 3.1415926F;
    cout << "PI: " << PI << endl;

    Data d{};
    cout << "d: " << d.toString() << endl;
    d.func();
    cout << "d: " << d.toString() << endl;
    return 0;
}
