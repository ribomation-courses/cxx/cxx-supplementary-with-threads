#include <iostream>
#include <string>

using namespace std;
using namespace std::literals;

#define interface       struct
#define abstract(f)     virtual f = 0
#define implements      /*noop*/

interface Stringable {
    abstract(string toString() const);
};

/** Java
 interface Stringable {
    String toString();
 }

 abstract class Stringable (extends java.lang.Object) {
    @Override
    abstract String toString();
 }
 */


struct Person : implements Stringable {
private:
    string name;
public:
    Person(const string& n) : name{n} {}

    virtual ~Person() = default;

    string toString() const override {
        return "name="s + name;
    }
};


struct Employee : Person {
    using super = Person;
private:
    string company;
public:
    Employee(const string& name, const string& company)
            : super{name}, company{company} {}

    string toString() const override {
        return "Employee{"s + super::toString() + ", company="s + company + "}"s;
    }

    void setCompany(const string& company) {
        Employee::company = company;
    }
};


struct Student : Person {
    using super = Person;
private:
    string school;
public:
    Student(const string& name, const string& school)
            : super{name}, school{school} {}

    string toString() const override {
        return "Student{"s + super::toString() + ", school="s + school + "}"s;
    }

    void setSchool(const string& school) {
        Student::school = school;
    }
};


void func(Stringable* p) {
    cout << "----\n[func] (1) p: " << p->toString() << endl;

    if (Employee* obj = dynamic_cast<Employee*>(p); obj != nullptr) {
        obj->setCompany("SAAB");
    }
    if (Student * obj = dynamic_cast<Student*>(p); obj != nullptr) {
        obj->setSchool("Chalmers");
    }

    cout << "[func] (2) p: " << p->toString() << endl;
}

int main() {
    Employee e{"Anna Conda"s, "Ericsson"s};
    Student  s{"Justin Time"s, "KTH"s};
    func(&e);
    func(&s);
    return 0;
}

