#include <iostream>
#include <string>
#include "account.hxx"

using namespace std;
using namespace std::literals;
using namespace ribomation;

int Account::instanceCount = 0;

int main() {

    cout << "[main] Account::count = " << Account::count() << endl;
    {
        Account a1{42};
        func(a1);
        cout << "[block] Account::count = " << Account::count() << endl;
        cout << "a1: " << a1 << endl;

        Account arr[5];
        cout << "[block] Account::count = " << Account::count() << endl;
        for (auto& a : arr) cout << "[block] arr: " << a << endl;
        cout << "[block] Account::count = " << Account::count() << endl;

        //for-loop
        const int N = sizeof(arr) / sizeof(arr[0]);
        for (int  k = 0; k < N; ++k) {
            Account localObject = arr[k];
            cout << "[for] arr: " << localObject << endl;
        }

    }
    cout << "[main] Account::count = " << Account::count() << endl;
    return 0;
}

