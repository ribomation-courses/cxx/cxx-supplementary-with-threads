#pragma once

#include <iosfwd>

namespace ribomation {
    using namespace std;

    class Account {
        static int instanceCount;
        int        balance = 0;
    public:
        Account() {
            ++instanceCount;
        }

        explicit Account(int initial) : balance{initial} {
            ++instanceCount;
        }

        Account(const Account& that) : balance{that.balance} {
            ++instanceCount;
        }

        ~Account() {
            --instanceCount;
        }

        static int count() {
            return instanceCount;
        }


        friend void func(const Account&) {
            cout << "func\n";
        }

        friend ostream& operator <<(ostream& os, const Account& a); /*{
            return os << "Account{balance=" << a.balance
                      << ", count=" << Account::count() << "}";
        }*/
    };

    ostream& operator <<(ostream& os, const Account& a) {
        return os << "Account{balance=" << a.balance
                  << ", count=" << Account::count() << "}";
    }

    // lhs << rhs:   ret operator(lhs, rhs)
    // lhs << rhs:   ret lhs.operator(rhs)

}



