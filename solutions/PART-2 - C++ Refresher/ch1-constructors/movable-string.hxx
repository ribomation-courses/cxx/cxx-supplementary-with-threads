#pragma once

#include <iosfwd>
#include <string>
#include <cstring>

namespace ribomation {
    using namespace std;
    using namespace std::literals;

    class MovableString {
        const char* payload = nullptr;
        unsigned size;

    public:
        MovableString() = delete;
        MovableString(const MovableString&) = delete;
        MovableString& operator =(const MovableString&) = delete;

        MovableString(const char* s) : payload{strdup(s)}, size(strlen(s)) {
            cout << "MovableString{" << s << ") @ " << this
                 << ", &payload=" << payload << ", size=" << size << endl;
        }

        ~MovableString() {
            cout << "~MovableString() @ " << this;
            if (payload != nullptr) {
                cout << ", deleting payload " << payload;
                delete payload;
            }
            cout << endl;
        }

        MovableString(MovableString&& that) : payload{that.payload}, size{that.size} {
            cout << "MovableString{&& " << that.payload << "} @ " << this
                 << ", &payload=" << payload << ", size=" << size << endl;
            that.payload = nullptr;
            that.size    = 0;
        }

        unsigned getSize() const { return size; }

        string toString() const {
            return " MovableString{"s + payload + ", size=" + to_string(size) + "}"s;
        }

        friend ostream& operator<<(ostream& os, const MovableString& s) {
            return os << s.toString();
        }
    };

}

