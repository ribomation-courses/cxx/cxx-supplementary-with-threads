#include <iostream>
#include "movable-string.hxx"

using namespace std;
using namespace std::literals;
using namespace ribomation;

void func(MovableString&& s) {
    cout << "[func] s=" << s << endl;
}


int main() {
    MovableString str{"Tjolla Hopp"};
    cout << "[main] str=" << str << endl;
    /*str = */func(move(str));
    cout << "[main] str=" << str << endl;
    return 0;
}