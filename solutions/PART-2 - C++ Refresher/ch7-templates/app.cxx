#include <iostream>
#include "simple-stack.hxx"
using namespace std;
using namespace std::literals;
using namespace ribomation;


void stack_of_ints() {
    cout << "stack<int>   : ";
    Stack<>   s;
    for (auto k = 1; !s.full(); ++k) s.push(k);
    while (!s.empty()) cout << s.pop() << " ";
    cout << endl;
}

void stack_of_string() {
    cout << "stack<string>: ";
    using namespace std::literals;
    Stack<string, 10> s;
    for (auto         k = s.capacity(); !s.full(); --k) s.push("txt-"s + to_string(k));
    while (!s.empty()) cout << s.pop() << " ";
    cout << endl;
}

struct Person {
    string name;
    Person(string name) : name{name} {}
    Person() = default;
    ~Person() = default;
    Person(const Person&) = default;
};

void stack_of_persons() {
    cout << "stack<Person>: ";
    using namespace std::literals;
    Stack<Person, 5> s;
    for (auto        k = s.capacity(); !s.full(); --k) s.push(Person{"Nisse-"s + to_string(k)});
    while (!s.empty()) cout << s.pop().name << " ";
    cout << endl;
}

int main() {
    stack_of_ints();
    stack_of_string();
    stack_of_persons();
    return 0;
}
