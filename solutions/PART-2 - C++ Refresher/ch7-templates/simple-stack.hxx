#pragma once

#include <iostream>
#include <string>

namespace ribomation {
    using namespace std;

    template<typename T = int, unsigned N = 20>
    class Stack {
        T        stk[N];
        unsigned top = 0;
    public:
        void     push(T x) { stk[top++] = x; }
        T        pop() { return stk[--top]; }
        bool     empty()    const { return top == 0; }
        bool     full()     const { return top >= N; }
        unsigned capacity() const { return N; }
    };

}

