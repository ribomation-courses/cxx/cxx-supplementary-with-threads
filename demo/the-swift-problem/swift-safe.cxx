#include <iostream>
#include <pthread.h>
#include "Swift.hxx"

class SafeMoneyBox : public MoneyBox {
    using super = MoneyBox;
    pthread_cond_t cond{};
    bool full = false;

public:
    SafeMoneyBox() {
        pthread_cond_init(&cond, nullptr);
    }

    ~SafeMoneyBox() {
        pthread_cond_destroy(&cond);
    }

    void send(int amount) override {
        Guard g{mutex};
        while (full) { pthread_cond_wait(&cond, mutex.native()); }
        full = true;
        payload = amount;
        pthread_cond_signal(&cond);
    }

    int recv() override {
        Guard g{mutex};
        while (!full) { pthread_cond_wait(&cond, mutex.native()); }
        full = false;
        auto amount = payload;
        pthread_cond_signal(&cond);
        return amount;
    }
};


int main(int argc, char* argv[]) {
    SWIFT<SafeMoneyBox> swift;
    swift.run(argc, argv);
    return 0;
}


