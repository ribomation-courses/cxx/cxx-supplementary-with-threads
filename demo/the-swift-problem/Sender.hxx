#pragma once

#include <iostream>
#include <sstream>
#include <string>
#include "Thread.hxx"
#include "MoneyBox.hxx"

using namespace std;
using namespace std::string_literals;
using namespace cxx_threads;

class SendingBank : public Thread {
    const unsigned numTransactions;
    MoneyBox& out;
    const int amount;
    const int stop;

public:
    SendingBank(unsigned numTransactions, MoneyBox& out, int amount = 1, int stop = -1) :
            numTransactions{numTransactions},
            out{out},
            amount{amount},
            stop{stop} {}

    unsigned getTotal() const {
        return numTransactions*amount;
    }

protected:
    void run() override {
        for (auto k = 0U; k < numTransactions; ++k) {
            out.send(amount);
        }
        out.send(stop);
    }
};


