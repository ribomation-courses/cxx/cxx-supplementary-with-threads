#include <iostream>
#include <pthread.h>
#include "ATM.hxx"

class SynchronizedAccount : public Account {
    using super = Account;
    pthread_mutex_t mutex{};

public:
    SynchronizedAccount() {
        pthread_mutex_init(&mutex, nullptr);
    }

    ~SynchronizedAccount() {
        pthread_mutex_destroy(&mutex);
    }

    void update(int amount) override {
        pthread_mutex_lock(&mutex);
        super::update(amount);
        pthread_mutex_unlock(&mutex);
    }
};

int main(int argc, char* argv[]) {
    ATM<SynchronizedAccount> atm;
    atm.run(argc, argv);
    return 0;
}


